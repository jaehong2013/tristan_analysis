#!/usr/bin/env python
"""
field_paper.py

by Jaehong Park (JaehongPark@lbl.gov)

This code provides a PySide based visualization tool
for the TRISTAN-MP data analysis.
base.py which provides the GUI framework is imported.
The QT-Desinger is used to generate base.ui. To convert base.ui to base.py, type
   pyside-uic -o base.py base.ui

##########################################
###### Install PySide and pyqtgraph ######
##########################################
To install PySide, you may type in the terminal window,
(1) conda install -c conda-forge pyside
or (2)  pip install -U PySide

Last updates:
9/2017

"""
import sys
import time as tm
#from PySide import QtCore, QtGui
import windowbase
from base import *
import matplotlib
matplotlib.rcParams['backend.qt4']='PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure

import glob
import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
import threading
import timeit

C = 2.99792458e8 # light speed
me = 9.10938291e-31 # electron mass
qe = 1.60217657e-19 # electron charge

# Define path way for TRISTAN-MP
file_path = '.'
param_list = glob.glob(file_path + '/param.???')
param_list.sort();
fld_list = glob.glob(file_path + '/flds.tot.???')
fld_list.sort();
prtl_list = glob.glob(file_path + '/prtl.tot.???')
prtl_list.sort();
spect_list = glob.glob(file_path + '/spect.???')
spect_list.sort();
spect2d_list = glob.glob(file_path + '/spect2d.???')
spect2d_list.sort();

tnum = len(param_list)
tstep = tnum

# get parameters
file = h5py.File(param_list[tstep-1], 'r')
mi = file['mi'][0];me = file['me'][0]
c = file['c'][0];c_omp = file['c_omp'][0]
delgam = file['delgam'][0];
istep = file['istep'][0];interval = file['interval'][0]
ppc0 = file['ppc0'][0]
dx = 1./c_omp;dt = c/c_omp
mx0 = file['mx0'][0];my0 = file['my0'][0];

#taxis = (np.arange(tnum)+1)*dt*interval
#time = taxis[tstep-1]
time = file['time'][0]

mass_dic = {}   # mass dictionary

mass_dic['mi'] = mi/me
mass_dic['me'] = me/me
        
species_list = []
species_list.append('electron')
species_list.append('ion')
nspecies = len(species_list)
        
print('e-ion mass ratio = ', mass_dic)
    
# find fields
fi = h5py.File(fld_list[0], 'r')
field_list_all = fi.items()
field_list = []
for field in field_list_all:
    field_list.append(field[0])
nfields = len(field_list)

xmin_factor = 0; xmax_factor = 100
ymin_factor = 0; ymax_factor = 100

# default values of global variables
animation = False
contrast1 = 100; contrast2 = 100
contrast3 = 100; contrast4 = 100
aspect = 'equal'

xwin1 = 10; xwin2 = 10
ywin1 = 30; ywin2 = 60

# This QmainWindow embeds matplotlib widgets
class MyPlot(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyPlot, self).__init__(parent)
        self.ui = windowbase.Ui_MainWindow()
        self.ui.setupUi(self)

        # set mainwindow title as the current directory
        self.setWindowTitle(os.getcwd())
        
        widget = self.geometry()
        xsize0 = widget.width()
        ysize0 = widget.height()
        # default margins for the plot panels
        #<----        xsize0            ------->
        #---------------------------------------|
        #                    y1                 |
        #               |---------------|       |
        #               |               |       | ysize0
        #< ---  x1 ---> |               |<- x2->|	
        #               |   plot panels |       |	
        #               |               |       |	
        #               |---------------|       |
        #                      y2               |
        #---------------------------------------|
        # size of each main plot panel
        mainwidth = (xsize0-(xwin1+xwin2))
        mainheight = 1.*(ysize0-(ywin1+ywin2))
        
        self.ui.widget1 = MatplotlibWidget(self.ui.centralwidget)
        self.ui.widget1.setGeometry(QtCore.QRect(xwin1, ywin1, mainwidth, mainheight))
        self.ui.widget1.setObjectName("widget")
                
    def resizeEvent(self,  event):
        
        width = event.size().width()
        height = event.size().height()
        mainwidth = (width-(xwin1+xwin2))
        mainheight = 1.*(height-(ywin1+ywin2))
        
        self.ui.widget1.setGeometry(QtCore.QRect(xwin1, ywin1, mainwidth, mainheight))
        
# This QmainWindow is for the control panel which contains function keys
class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyForm, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.setWindowTitle('Control panel')
        
        widget = self.geometry()
        width = widget.width()
        height = widget.height()
        # re-position the control panel
        self.setGeometry(200, 100, width, height)
        
        # show the plot window
        self.myplot = MyPlot()
        self.myplot.show()
        
        widget2 = self.myplot.geometry()
        width2 = widget2.width()
        height2 = widget2.height()
        # re-position the plot window
        self.myplot.setGeometry(200+width, 100, width2, height2)
        
        # emit signal to open directory
        self.connect(self.ui.actionOpen, QtCore.SIGNAL('triggered()' ), self.opendir)
        self.connect(self.ui.actionClose, QtCore.SIGNAL('triggered()' ), self.close)	

        # time label
        self.ui.tstepLabel.setText("%d" %tstep)
        self.ui.timeLabel.setText("%6.1f" % time)

        # timestep stride spin box
        self.ui.stepSpinBox.setValue(1)
        self.ui.stepSpinBox.setMinimum(1)

        # backward time button
        self.ui.backwardtimeButton.clicked.connect(self.backwardtimebutton)
        # foward time button
        self.ui.forwardtimeButton.clicked.connect(self.forwardtimebutton)
        
        # timestep slider
        self.ui.timeSlider.setRange(1,tnum)
        self.ui.timeSlider.setSingleStep(1)
        self.ui.timeSlider.setValue(tnum)
        self.ui.timeSlider.valueChanged.connect(self.timeslider)
        
        self.ui.xminLabel.setText(str("%d" %0 )+"%")
        self.ui.xminSlider.setRange(0,100)
        self.ui.xmaxSlider.setValue(0)
        self.ui.xminSlider.valueChanged.connect(self.xminslider)
        self.ui.xmaxLabel.setText(str("%d" %100)+"%")
        self.ui.xmaxSlider.setRange(0,100)
        self.ui.xmaxSlider.setValue(100)
        self.ui.xmaxSlider.valueChanged.connect(self.xmaxslider)
        
        self.ui.yminLabel.setText(str("%d" %0)+"%")
        self.ui.yminSlider.setRange(0,100)
        self.ui.ymaxSlider.setValue(0)
        self.ui.yminSlider.valueChanged.connect(self.yminslider)
        self.ui.ymaxLabel.setText(str("%d" %100)+"%")
        self.ui.ymaxSlider.setRange(0,100)
        self.ui.ymaxSlider.setValue(100)
        self.ui.ymaxSlider.valueChanged.connect(self.ymaxslider)
        
  
        # plot PushButton
        self.ui.plotButton.clicked.connect(self.plotbutton)
        
        # animation PushButton
        self.ui.animationButton.clicked.connect(self.animationbutton)
        self.ui.tiniSpinBox.setMinimum(1)
        self.ui.tiniSpinBox.setMaximum(tnum)
        self.ui.tiniSpinBox.setValue(1)
        self.ui.tmaxSpinBox.setMinimum(1)
        self.ui.tmaxSpinBox.setMaximum(tnum)
        self.ui.tmaxSpinBox.setValue(tnum)
                
        # select image CheckBox
        self.ui.pngCheckBox.setChecked(False)
        self.ui.pngCheckBox.clicked.connect(self.pngcheckbox)
        self.ui.epsCheckBox.setChecked(False)
        self.ui.epsCheckBox.clicked.connect(self.epscheckbox)
        self.ui.movieCheckBox.setChecked(False)
        self.ui.movieCheckBox.clicked.connect(self.moviecheckbox)
        
        
        # contrast sliders
        self.ui.contrastSlider1.setRange(1,150)
        self.ui.contrastSlider1.setValue(100)
        self.ui.contrastLabel1.setText(str("%d" %100 )+"%")
        self.ui.contrastSlider1.valueChanged.connect(self.contrastslider1)
        self.ui.contrastSlider2.setRange(1,150)
        self.ui.contrastSlider2.setValue(100)
        self.ui.contrastLabel2.setText(str("%d" %100 )+"%")
        self.ui.contrastSlider2.valueChanged.connect(self.contrastslider2)
        self.ui.contrastSlider3.setRange(1,150)
        self.ui.contrastSlider3.setValue(100)
        self.ui.contrastLabel3.setText(str("%d" %100 )+"%")
        self.ui.contrastSlider3.valueChanged.connect(self.contrastslider3)
        self.ui.contrastSlider4.setRange(1,150)
        self.ui.contrastSlider4.setValue(100)
        self.ui.contrastLabel4.setText(str("%d" %100 )+"%")
        self.ui.contrastSlider4.valueChanged.connect(self.contrastslider4)
        
        # return original contrast button
        self.ui.returncontrastButton1.clicked.connect(self.returncontrastbutton1)
        self.ui.returncontrastButton2.clicked.connect(self.returncontrastbutton2)
        self.ui.returncontrastButton3.clicked.connect(self.returncontrastbutton3)
        self.ui.returncontrastButton4.clicked.connect(self.returncontrastbutton4)
                
        self.plotbutton()
    
	# open a new directory and load files for basic information
    def opendir(self):	
        pass
        
    def close(self):
        sys.exit(0)
              
    def backwardtimebutton(self):
        step = self.ui.stepSpinBox.value()
        tstep = self.ui.timeSlider.value()
        tstep = tstep - step
        if tstep < 1:
            tstep = tstep + step
        self.ui.tstepLabel.setText("%d" %tstep)
        time = taxis[tstep-1]    
        self.ui.timeLabel.setText("%6.1f" % time)
        self.ui.timeSlider.setValue(tstep)
        self.plotbutton()
        
    def forwardtimebutton(self):
        step = self.ui.stepSpinBox.value()
        tstep=self.ui.timeSlider.value()
        tstep = tstep + step
        if tstep > tnum:
            tstep = tstep - step
        self.ui.tstepLabel.setText("%d" %tstep)
        time = taxis[tstep-1]    
        self.ui.timeLabel.setText("%6.1f" % time)
        self.ui.timeSlider.setValue(tstep)
        self.plotbutton()
       
    def timeslider(self):
        tstep=self.ui.timeSlider.value()
        self.ui.tstepLabel.setText("%d" %tstep)
        self.ui.timeLabel.setText("%6.1f" % time)
        
    def xminslider(self):
        global xmin_factor
        xmin_factor = self.ui.xminSlider.value()
        xmax_factor = self.ui.xmaxSlider.value()
        if xmin_factor >= xmax_factor: 
            xmin_factor = xmax_factor - 1
        self.ui.xminSlider.setValue(xmin_factor)
        xmin = xmin0+(xmax0-xmin0)*xmin_factor/100 
        self.ui.xminLabel.setText(str("%d" %xmin_factor)+"%"
            +" = "+str("%.1f" %xmin))
            
    def xmaxslider(self):
        global xmax_factor
        xmin_factor = self.ui.xminSlider.value()
        xmax_factor = self.ui.xmaxSlider.value()
        if xmax_factor <= xmin_factor: 
            xmax_factor = xmin_factor + 1
        self.ui.xmaxSlider.setValue(xmax_factor)
        xmax = xmin0+(xmax0-xmin0)*xmax_factor/100 
        self.ui.xmaxLabel.setText(str("%d" %xmax_factor)+"%"
            +" = "+str("%.1f" %xmax))
            
    def yminslider(self):
        global ymin_factor
        ymin_factor = self.ui.yminSlider.value()
        ymax_factor = self.ui.ymaxSlider.value()
        if ymin_factor >= ymax_factor:
            ymin_factor = ymax_factor - 1
        self.ui.yminSlider.setValue(ymin_factor)
        ymin = ymin0+(ymax0-ymin0)*ymin_factor/100 
        self.ui.yminLabel.setText(str("%d" %ymin_factor)+"%"
            +" = "+str("%.1f" %ymin))
            
    def ymaxslider(self):
        global ymax_factor
        ymin_factor = self.ui.yminSlider.value()
        ymax_factor = self.ui.ymaxSlider.value()
        if ymax_factor <= ymin_factor: 
            ymax_factor = ymin_factor + 1
        self.ui.ymaxSlider.setValue(ymax_factor)
        ymax = ymin0+(ymax0-ymin0)*ymax_factor/100
        self.ui.ymaxLabel.setText(str("%d" %ymax_factor)+"%"
            +" = "+str("%.1f" %ymax))
        
 
    def pngcheckbox(self):
        if self.ui.pngCheckBox.isChecked():
            self.ui.epsCheckBox.setChecked(False)
            self.ui.movieCheckBox.setChecked(False)
            
    def epscheckbox(self):
        if self.ui.epsCheckBox.isChecked():
            self.ui.pngCheckBox.setChecked(False)
            self.ui.movieCheckBox.setChecked(False)
            
    def moviecheckbox(self):
        if self.ui.movieCheckBox.isChecked():
            self.ui.pngCheckBox.setChecked(False)
            self.ui.epsCheckBox.setChecked(False)
            os.system('mkdir ../movie_field_paper')
                
    def plotbutton(self):
            
        if animation == False:
        
            tstep = self.ui.timeSlider.value()
                   
            if self.ui.pngCheckBox.isChecked(): saveimg = 1
            elif self.ui.epsCheckBox.isChecked(): saveimg = 2
            else:   saveimg = 0
            
            self.myplot.ui.widget1.field(tstep, saveimg)
        
    def myEvenListener(self):
        global animation
        animation = True
        
        if self.ui.movieCheckBox.isChecked(): 
            saveimg = 3
            image_dir = '..'
            os.system('mkdir '+image_dir+'/movie_field_paper/')
                
        else: saveimg = 0
        global tini, tmax
        tini = self.ui.tiniSpinBox.value()
        tmax = self.ui.tmaxSpinBox.value()
        step = self.ui.stepSpinBox.value()
        # number of iterations in animation
        num = (tmax-tini)/step+1
        if num<0: num=1
        tarr = np.zeros(num, dtype=np.int)
        t = tmax
        for i in np.arange(num)[::-1]:
            tarr[i] = t
            t = t - step
        for tstep in tarr:
            tm.sleep(0.005)
            self.ui.tstepLabel.setText("%d" %tstep)
            self.ui.timeLabel.setText("%6.1f $/\omega_{pe}$" % time)
            self.ui.timeSlider.setValue(tstep)
            self.myplot.ui.widget1.field(tstep, saveimg)
            tstep = tstep + step
        # When animation is done, animation = False
        animation = False
        
    def animationbutton(self):
        # while animation is running, this function is skipped.
        if animation == False:
            self.c_thread=threading.Thread(target=self.myEvenListener)
            self.c_thread.start()
        else:
            pass
                                    
    def contrastslider1(self):
        global contrast1
        contrast1 = self.ui.contrastSlider1.value()
        self.ui.contrastLabel1.setText(str("%d" %contrast1)+"%")
    def contrastslider2(self):
        global contrast2
        contrast2 = self.ui.contrastSlider2.value()
        self.ui.contrastLabel2.setText(str("%d" %contrast2)+"%")
    def contrastslider3(self):
        global contrast3
        contrast3 = self.ui.contrastSlider3.value()
        self.ui.contrastLabel3.setText(str("%d" %contrast3)+"%")  
    def contrastslider4(self):
        global contrast4
        contrast4 = self.ui.contrastSlider4.value()
        self.ui.contrastLabel4.setText(str("%d" %contrast4)+"%")
    
    
        
    def returncontrastbutton1(self):
        global contrast1
        contrast1 = 100
        self.ui.contrastSlider1.setValue(contrast1)
        self.ui.contrastLabel1.setText(str("%d" %contrast1)+"%")    
        self.plotbutton()
    def returncontrastbutton2(self):
        global contrast2
        contrast2 = 100
        self.ui.contrastSlider2.setValue(contrast2)
        self.ui.contrastLabel2.setText(str("%d" %contrast2)+"%")    
        self.plotbutton()
    def returncontrastbutton3(self):
        global contrast3
        contrast3 = 100
        self.ui.contrastSlider3.setValue(contrast3)
        self.ui.contrastLabel3.setText(str("%d" %contrast3)+"%")    
        self.plotbutton()
    def returncontrastbutton4(self):
        global contrast4
        contrast4 = 100
        self.ui.contrastSlider3.setValue(contrast3)
        self.ui.contrastLabel3.setText(str("%d" %contrast4)+"%")    
        self.plotbutton()
                    
        
class MatplotlibWidget(Canvas):
    
    def __init__(self, parent=None):		
        super(MatplotlibWidget, self).__init__(Figure())
        
        self.setParent(parent)
        self.figure = Figure(dpi=60)
        self.canvas = Canvas(self.figure)
        self.figure.set_facecolor('0.85')
        
    def field(self, tstep, saveimg):
                                                         
        self.figure.clear()
        
        global xmin0, xmax0, ymin0, ymax0
        ########################################
        file = h5py.File(param_list[tstep-1], 'r')
        time = file['time'][0]
        # read hdf5 field data
        field1 = 'dens'
        field2 = 'v3xi'
        field3 = 'v3yi'
        fi = h5py.File(fld_list[tstep-1], 'r')
        fdata1 = fi[field1]/ppc0
        fdata2 = fi[field2]
        fdata3 = fi[field3]
        fdata1 = fdata1[0,:,:]
        fdata2 = fdata2[0,:,:]
        fdata3 = fdata3[0,:,:]
        ########################################
        ny, nx = np.shape(fdata1)
        
        xmin0 = 0.; xmax0 = nx*istep*dx/np.sqrt(mi/me)
        ymin0 = 0.; ymax0 = ny*istep*dx/np.sqrt(mi/me)
        
        xaxis=1.*np.arange(nx)*(xmax0-xmin0)/(nx-1)+xmin0
        yaxis=1.*np.arange(ny)*(ymax0-ymin0)/(ny-1)+ymin0
         
        # zoom-in/out
        xmin = xmin0+(xmax0-xmin0)*xmin_factor/100
        xmax = xmin0+(xmax0-xmin0)*xmax_factor/100
        ymin = ymin0+(ymax0-ymin0)*ymin_factor/100 
        ymax = ymin0+(ymax0-ymin0)*ymax_factor/100
        
        # zoom-in/out indices
        iL = np.where(xaxis >= xmin)[0]
        if len(iL) != 0: iL=iL[0]
        else: iL = 0
        iR = np.where(xaxis >= xmax)[0]
        if len(iR) != 0: iR=iR[0]
        else: iR = nx-1
        jL = np.where(yaxis >= ymin)[0]
        if len(jL) != 0: jL=jL[0]
        else: jL = 0
        jR = np.where(yaxis >= ymax)[0]
        if len(jR) != 0: jR=jR[0]
        else: jR = ny-1

        
        nbin = 500
        fontsize0 = 14; fontsize1 = 15
        cbarwidth = 0.16
                
        # Main plot
        self.plot = self.figure.add_subplot(131)
        self.plot.cla()
        vmin = np.min(fdata1[jL:jR,iL:iR])
        vmin = vmin*contrast1/100.
        vmax = np.max(fdata1[jL:jR,iL:iR])
        vmax = vmax*contrast1/100.
        im =  self.plot.imshow(fdata1[jL:jR,iL:iR], interpolation='nearest', cmap='jet',
            origin='lower', extent=[ xmin,xmax,ymin,ymax ], 
            vmin=vmin, vmax=vmax, aspect=aspect)
        ax1 = self.figure.gca()
        ax1.text(xmin+1,ymax-8, '(a) $n/n_0$', color='white', fontsize=16)
        divider = make_axes_locatable(ax1)
        cax = divider.append_axes("top",size=cbarwidth, pad=0.25)
        cb = self.figure.colorbar(im, cax=cax, orientation='horizontal',)
        tick_locator = ticker.MaxNLocator(nbins=5)
        cb.locator = tick_locator
        cb.update_ticks()
        #if saveimg == 0:
        #    self.plot.set_title(field1, fontsize=fontsize0)
        #else:
        #    time = taxis[tstep-1]
        #    self.plot.set_title(field1+'   t=%6.1f $/\omega_{pe}$' % time, fontsize=fontsize0)
        self.plot.set_xlabel('x ($c/\omega_{pi}$)', fontsize=fontsize1)
        self.plot.set_ylabel('y ($c/\omega_{pi}$)', fontsize=fontsize1)
        self.plot.axes.set_xlim([xmin,xmax])
        self.plot.axes.set_ylim([ymin,ymax])
        #self.plot.axes.get_figure().tight_layout()
        
        self.plot = self.figure.add_subplot(132)
        self.plot.cla()
        vmin = np.min(fdata2[jL:jR,iL:iR])
        vmin = vmin*contrast2/100.
        vmax = np.max(fdata2[jL:jR,iL:iR])
        vmax = vmax*contrast2/100.
        im =  self.plot.imshow(fdata2[jL:jR,iL:iR], interpolation='nearest', cmap='jet',
            origin='lower', extent=[ xmin,xmax,ymin,ymax ], vmin=vmin, vmax=vmax, aspect=aspect)
        ax2 = self.figure.gca()
        ax2.text(xmin+1,ymax-8, '(b) $V_x/c$', color='black', fontsize=16)
        divider = make_axes_locatable(ax2)
        cax = divider.append_axes("top", size=cbarwidth, pad=0.25)
        cb = self.figure.colorbar(im, cax=cax, orientation='horizontal',)
        tick_locator = ticker.MaxNLocator(nbins=4)
        cb.locator = tick_locator
        cb.update_ticks()
        #self.plot.set_title(field2+'   t=%6.1f $/\omega_{pe}$' % time, fontsize=fontsize0)
        self.plot.set_xlabel('x ($c/\omega_{pi}$)', fontsize=fontsize1)
        #self.plot.set_ylabel('y ($c/\omega_{pi}$)', fontsize=fontsize1)
        self.plot.axes.set_xlim([xmin,xmax])
        self.plot.axes.set_ylim([ymin,ymax])
        #self.plot.axes.get_figure().tight_layout()
        #self.plot.set_yticklabels([])
        
        self.plot = self.figure.add_subplot(133)
        self.plot.cla()
        vmin = np.min(fdata3[jL:jR,iL:iR])
        vmin = vmin*contrast3/100.
        vmax = np.max(fdata3[jL:jR,iL:iR])
        vmax = vmax*contrast3/100.
        im =  self.plot.imshow(fdata3[jL:jR,iL:iR], interpolation='nearest', cmap='jet',
            origin='lower', extent=[ xmin,xmax,ymin,ymax ], vmin=vmin, vmax=vmax, aspect=aspect)
        ax3 = self.figure.gca()
        ax3.text(xmin+1,ymax-8, '(c) $V_y/c$', color='black', fontsize=16)
        divider = make_axes_locatable(ax3)
        cax = divider.append_axes("top", size=cbarwidth, pad=0.25)
        cb = self.figure.colorbar(im, cax=cax, orientation='horizontal',)
        tick_locator = ticker.MaxNLocator(nbins=4)
        cb.locator = tick_locator
        cb.update_ticks()
        #self.plot.set_title(field3+'   t=%6.1f $/\omega_{pe}$' % time, fontsize=fontsize0)
        self.plot.set_xlabel('x ($c/\omega_{pi}$)', fontsize=fontsize1)
        #self.plot.set_ylabel('y ($c/\omega_{pi}$)', fontsize=fontsize1)
        self.plot.axes.set_xlim([xmin,xmax])
        self.plot.axes.set_ylim([ymin,ymax])
        #self.plot.axes.get_figure().tight_layout()        
        #self.plot.set_yticklabels([])  
        self.draw()
            
        if saveimg !=0:
            image_dir = '..'
            if saveimg == 1:
                self.figure.savefig(image_dir+'/field_paper%3.3d.png' %tstep, format='png')  
                print(image_dir+'/field_paper%3.3d.png' %tstep)  
            if saveimg == 2:
                self.figure.savefig(image_dir+'/field_paper%3.3d.eps' %tstep, format='eps', dpi=60)
                print(image_dir+'/field_paper%3.3d.eps' %tstep)  
            if saveimg == 3 and animation == True:
                self.figure.savefig(image_dir+'/movie_field_paper/'+'/field_paper%3.3d.png' %tstep, format='png')
                print(image_dir+'/movie_field_paper/'+'/field_paper%3.3d.png' %tstep) 
    

if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	myapp = MyForm()
	myapp.show()
	sys.exit(app.exec_())