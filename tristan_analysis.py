#!/usr/bin/env python
"""
tristan_analysis.py

by Jaehong Park (JaehongPark@lbl.gov)

This code provides a PySide based visualization tool
for the TRISTAN-MP data analysis.
base.py which provides the GUI framework is imported.
The QT-Desinger is used to generate base.ui. To convert base.ui to base.py, type
   pyside-uic -o base.py base.ui

##########################################
###### Install PySide and pyqtgraph ######
##########################################
To install PySide, you may type in the terminal window,
(1) conda install -c conda-forge pyside
or (2)  pip install -U PySide
For Mac OS-X users, use (2) so that the pyqtgraph library works properly.

On Nersc or other clusters,
you can install PySide in the following way:
(1) download miniconda2
wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
(2) install miniconda2
bash Miniconda2-latest-Linux-x86_64.sh
(3) activate miniconda2
source miniconda2/bin/activate
(4) finally, install PySide using conda,
conda install -c conda-forge pyside

To install pyqtgraph, type
    pip install pyqtgraph"
or visit http://www.pyqtgraph.org for instruction in detail.

###############################################
###### Download tristan_analysis package ######
###############################################
If you have installed PySide and pyqtgraph, download warpx_analysis.py from the git repository,
    git clone https://jaehong2013@bitbucket.org/jaehong2013/warpx_pyside.git
and go to the folder where your simulation data files are saved, and type
     ~/tristan_analysis/tristan_analysis.py



Last updates:
8/2017

"""
import sys
import time as tm
#from PySide import QtCore, QtGui
import windowbase
from base import *
import matplotlib
matplotlib.rcParams['backend.qt4']='PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as Canvas
from matplotlib.figure import Figure

import glob
import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
import threading
import timeit

C = 2.99792458e8 # light speed
me = 9.10938291e-31 # electron mass
qe = 1.60217657e-19 # electron charge

# Define path way for TRISTAN-MP
file_path = '.'
param_list = glob.glob(file_path + '/param.???')
param_list.sort();
fld_list = glob.glob(file_path + '/flds.tot.???')
fld_list.sort();
prtl_list = glob.glob(file_path + '/prtl.tot.???')
prtl_list.sort();
spect_list = glob.glob(file_path + '/spect.???')
spect_list.sort();


tnum = len(param_list)
tstep = tnum

# get parameters
file = h5py.File(param_list[tstep-1], 'r')
mi = file['mi'][0];me = file['me'][0]
c = file['c'][0];c_omp = file['c_omp'][0]
delgam = file['delgam'][0];
istep = file['istep'][0];interval = file['interval'][0]
dx = 1./c_omp;dt = c/c_omp
mx0 = file['mx0'][0];my0 = file['my0'][0];

taxis = (np.arange(tnum)+1)*dt*interval
time = taxis[tstep-1]

mass_dic = {}   # mass dictionary

mass_dic['mi'] = mi/me
mass_dic['me'] = me/me
        
species_list = []
species_list.append('electron')
species_list.append('ion')
nspecies = len(species_list)
        
print('e-ion mass ratio = ', mass_dic)
    
# find fields
fi = h5py.File(fld_list[0], 'r')
field_list_all = fi.items()
field_list = []
for field in field_list_all:
    field_list.append(field[0])
nfields = len(field_list)


# phase space variables
phase_list = []
phase_list.append('x-y')
phase_list.append('f(E)-log-lin')
phase_list.append('f(E)-log-log')
phase_list.append('x-px')
phase_list.append('x-py')
phase_list.append('x-pz')
phase_list.append('px-py')
phase_list.append('x-ene')
nphase = len(phase_list)

# list local phase space variables
localphase_list = []
localphase_list.append('x-y')
localphase_list.append('FFT')
localphase_list.append('density')
localphase_list.append('f(E)-log-lin')
localphase_list.append('f(E)-log-log')
localphase_list.append('x-py')
localphase_list.append('x-py')
localphase_list.append('x-pz')
localphase_list.append('px-py')
localphase_list.append('x-ene')
localphase_list.append('MaxE')
localphase_list.append('spece')
localphase_list.append('speci')
nlocalphase = len(localphase_list)

xmin_factor = 0; xmax_factor = 100
ymin_factor = 0; ymax_factor = 100

# default values of global variables
animation = False
oned1 = False; oned2 = False
aspect1 = 'equal'; aspect2 = 'equal'
count1 = 0; count2 = 0
xi1_factor=xf1_factor=yi1_factor=yf1_factor=0
xi2_factor=xf2_factor=yi2_factor=yf2_factor=0
lineselect1 = False; lineselect2 = False;
tempfit1 = 0; tempfit2 =0;
temp1 = 0.; temp2 = 0.;
norm1 = 1.0; norm2 = 1.0
pxmin1 = 0; pxmax1 = 99
pymin1 = 0; pymax1 = 99
pxmin2 = 0; pxmax2 = 99
pymin2 = 0; pymax2 = 99
window1 = 1; window2 = 2
contrast1 = 100; contrast2 = 100

xwin1 = 10; xwin2 = 10
ywin1 = 30; ywin2 = 60

# This QmainWindow embeds matplotlib widgets
class MyPlot(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyPlot, self).__init__(parent)
        self.ui = windowbase.Ui_MainWindow()
        self.ui.setupUi(self)

        # set mainwindow title as the current directory
        self.setWindowTitle(os.getcwd())
        
        widget = self.geometry()
        xsize0 = widget.width()
        ysize0 = widget.height()
        # default margins for the plot panels
        #<----        xsize0            ------->
        #---------------------------------------|
        #                    y1                 |
        #               |---------------|       |
        #               |               |       | ysize0
        #< ---  x1 ---> |               |<- x2->|	
        #               |   plot panels |       |	
        #               |               |       |	
        #               |---------------|       |
        #                      y2               |
        #---------------------------------------|
        # size of each main plot panel
        mainwidth = (xsize0-(xwin1+xwin2))
        mainheight = 1.*(ysize0-(ywin1+ywin2))/2
        
        self.ui.widget1 = MatplotlibWidget(self.ui.centralwidget)
        self.ui.widget1.setGeometry(QtCore.QRect(xwin1, ywin1, mainwidth, mainheight))
        self.ui.widget1.setObjectName("widget")
        
        self.ui.widget2 = MatplotlibWidget(self.ui.centralwidget)
        self.ui.widget2.setGeometry(QtCore.QRect(xwin1, ywin1+1.*mainheight, mainwidth, mainheight))
        self.ui.widget2.setObjectName("widget")
        
    def resizeEvent(self,  event):
        
        width = event.size().width()
        height = event.size().height()
        mainwidth = (width-(xwin1+xwin2))
        mainheight = 1.*(height-(ywin1+ywin2))/2
        
        self.ui.widget1.setGeometry(QtCore.QRect(xwin1, ywin1, mainwidth, mainheight))
        self.ui.widget2.setGeometry(QtCore.QRect(xwin1, ywin1+1.*mainheight, mainwidth, mainheight))
        
# This QmainWindow is for the control panel which contains function keys
class MyForm(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyForm, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.setWindowTitle('Control panel')
        
        widget = self.geometry()
        width = widget.width()
        height = widget.height()
        # re-position the control panel
        self.setGeometry(200, 100, width, height)
        
        # show the plot window
        self.myplot = MyPlot()
        self.myplot.show()
        
        widget2 = self.myplot.geometry()
        width2 = widget2.width()
        height2 = widget2.height()
        # re-position the plot window
        self.myplot.setGeometry(200+width, 100, width2, height2)
        
        # emit signal to open directory
        self.connect(self.ui.actionOpen, QtCore.SIGNAL('triggered()' ), self.opendir)
        self.connect(self.ui.actionClose, QtCore.SIGNAL('triggered()' ), self.close)	
        # (x,y) coordinate labeli
        self.ui.coordLabel.setText(str('(0,0)'))

        # time label
        self.ui.tstepLabel.setText("%d" %tstep)
        self.ui.timeLabel.setText("%6.1f" % time)

        # timestep stride spin box
        self.ui.stepSpinBox.setValue(1)
        self.ui.stepSpinBox.setMinimum(1)

        # backward time button
        self.ui.backwardtimeButton.clicked.connect(self.backwardtimebutton)
        # foward time button
        self.ui.forwardtimeButton.clicked.connect(self.forwardtimebutton)
        
        # timestep slider
        self.ui.timeSlider.setRange(1,tnum)
        self.ui.timeSlider.setSingleStep(1)
        self.ui.timeSlider.setValue(tnum)
        self.ui.timeSlider.valueChanged.connect(self.timeslider)
        
        self.ui.xminLabel.setText(str("%d" %0 )+"%")
        self.ui.xminSlider.setRange(0,100)
        self.ui.xmaxSlider.setValue(0)
        self.ui.xminSlider.valueChanged.connect(self.xminslider)
        self.ui.xmaxLabel.setText(str("%d" %100)+"%")
        self.ui.xmaxSlider.setRange(0,100)
        self.ui.xmaxSlider.setValue(100)
        self.ui.xmaxSlider.valueChanged.connect(self.xmaxslider)
        
        self.ui.yminLabel.setText(str("%d" %0)+"%")
        self.ui.yminSlider.setRange(0,100)
        self.ui.ymaxSlider.setValue(0)
        self.ui.yminSlider.valueChanged.connect(self.yminslider)
        self.ui.ymaxLabel.setText(str("%d" %100)+"%")
        self.ui.ymaxSlider.setRange(0,100)
        self.ui.ymaxSlider.setValue(100)
        self.ui.ymaxSlider.valueChanged.connect(self.ymaxslider)
        
           
        # field and particle select buttons for three plot panels
        self.ui.fieldButton1.setChecked(True)
        self.ui.fieldButton2.setChecked(True)
        QtCore.QObject.connect(self.ui.fieldButton1, QtCore.SIGNAL('clicked()'),self.plotbutton)
        QtCore.QObject.connect(self.ui.fieldButton2, QtCore.SIGNAL('clicked()'),self.plotbutton)
        QtCore.QObject.connect(self.ui.particleButton1, QtCore.SIGNAL('clicked()'),self.plotbutton)
        QtCore.QObject.connect(self.ui.particleButton2, QtCore.SIGNAL('clicked()'),self.plotbutton)
        
		# fieldcombo-boxes
        for i in np.arange(nfields):
            self.ui.fieldsComboBox1.addItem(field_list[i], i)
            self.ui.fieldsComboBox2.addItem(field_list[i], i)
        self.ui.fieldsComboBox1.setCurrentIndex(0)
        self.ui.fieldsComboBox2.setCurrentIndex(2)
        self.ui.fieldsComboBox1.currentIndexChanged.connect(self.fieldcombobox1)
        self.ui.fieldsComboBox2.currentIndexChanged.connect(self.fieldcombobox2)
        
		# particle species combo-boxes
        for i in np.arange(nspecies):
            self.ui.particlesComboBox1.addItem(species_list[i], i)
            self.ui.particlesComboBox2.addItem(species_list[i], i)
        self.ui.particlesComboBox1.currentIndexChanged.connect(self.particlecombobox1)
        self.ui.particlesComboBox2.currentIndexChanged.connect(self.particlecombobox2)
  	
        # particle phase space combo-boxes
        for i in np.arange(nphase):
            self.ui.phaseComboBox1.addItem(phase_list[i], i)
            self.ui.phaseComboBox2.addItem(phase_list[i], i)
        self.ui.phaseComboBox1.currentIndexChanged.connect(self.plotbutton)
        self.ui.phaseComboBox2.currentIndexChanged.connect(self.plotbutton)
        
        for i in np.arange(nlocalphase):
            self.ui.localphaseComboBox1.addItem(localphase_list[i], i)
            self.ui.localphaseComboBox2.addItem(localphase_list[i], i)
        self.ui.localphaseComboBox1.currentIndexChanged.connect(self.localdatapushbutton1) 
        self.ui.localphaseComboBox2.currentIndexChanged.connect(self.localdatapushbutton2) 
            
        # 1D checkbox
        self.ui.onedCheckBox1.setChecked(False)
        self.ui.onedCheckBox1.clicked.connect(self.onedcheckbox1)
        self.ui.onedCheckBox2.setChecked(False)    
        self.ui.onedCheckBox2.clicked.connect(self.onedcheckbox2)    
            
        # aspect ratio checkbox
        self.ui.aspectCheckBox1.setChecked(True)
        self.ui.aspectCheckBox1.clicked.connect(self.aspectcheckbox1)
        self.ui.aspectCheckBox2.setChecked(True) 
        self.ui.aspectCheckBox2.clicked.connect(self.aspectcheckbox2)
        
        # line selection checkbox
        self.ui.lineCheckBox1.setChecked(False)
        self.ui.lineCheckBox1.clicked.connect(self.linecheckbox1)
        self.ui.lineCheckBox2.setChecked(False)
        self.ui.lineCheckBox2.clicked.connect(self.linecheckbox2)
        
        # rectangle selection checkbox
        self.ui.rectangleCheckBox1.setChecked(False)
        self.ui.rectangleCheckBox1.clicked.connect(self.rectanglecheckbox1)
        self.ui.rectangleCheckBox2.setChecked(False)
        self.ui.rectangleCheckBox2.clicked.connect(self.rectanglecheckbox2)
         
        # localdata push button
        self.ui.localdataPushButton1.clicked.connect(self.localdatapushbutton1)     
        self.ui.localdataPushButton2.clicked.connect(self.localdatapushbutton2)        
            
        # temperature fitting checkbox
        self.ui.TempEdit1.setText(str(temp1))
        self.ui.NormEdit1.setText(str(norm1))
        self.ui.T2dCheckBox1.setChecked(False)
        self.ui.T2dCheckBox1.clicked.connect(self.t2dcheckbox1)
        self.ui.T3dCheckBox1.setChecked(False)
        self.ui.T3dCheckBox1.clicked.connect(self.t3dcheckbox1)
        self.ui.TempEdit2.setText(str(temp2))
        self.ui.NormEdit2.setText(str(norm2))
        self.ui.T2dCheckBox2.setChecked(False)
        self.ui.T2dCheckBox2.clicked.connect(self.t2dcheckbox2)
        self.ui.T3dCheckBox2.setChecked(False)
        self.ui.T3dCheckBox2.clicked.connect(self.t3dcheckbox2)
            
        # plot PushButton
        self.ui.plotButton.clicked.connect(self.plotbutton)
        
        # animation PushButton
        self.ui.animationButton.clicked.connect(self.animationbutton)
        self.ui.tiniSpinBox.setMinimum(1)
        self.ui.tiniSpinBox.setMaximum(tnum)
        self.ui.tiniSpinBox.setValue(1)
        self.ui.tmaxSpinBox.setMinimum(1)
        self.ui.tmaxSpinBox.setMaximum(tnum)
        self.ui.tmaxSpinBox.setValue(tnum)
                
        # select image CheckBox
        self.ui.pngCheckBox.setChecked(False)
        self.ui.pngCheckBox.clicked.connect(self.pngcheckbox)
        self.ui.epsCheckBox.setChecked(False)
        self.ui.epsCheckBox.clicked.connect(self.epscheckbox)
        self.ui.movieCheckBox.setChecked(False)
        self.ui.movieCheckBox.clicked.connect(self.moviecheckbox)
        
        
        # contrast sliders
        self.ui.contrastSlider1.setRange(1,150)
        self.ui.contrastSlider1.setValue(100)
        self.ui.contrastLabel1.setText(str("%d" %100 )+"%")
        self.ui.contrastSlider1.valueChanged.connect(self.contrastslider1)
        self.ui.contrastSlider2.setRange(1,150)
        self.ui.contrastSlider2.setValue(100)
        self.ui.contrastLabel2.setText(str("%d" %100 )+"%")
        self.ui.contrastSlider2.valueChanged.connect(self.contrastslider2)
        
        # return original contrast button
        self.ui.returncontrastButton1.clicked.connect(self.returncontrastbutton1)
        self.ui.returncontrastButton2.clicked.connect(self.returncontrastbutton2)
        
        # pzmin and pzmax sliders
        self.ui.pxminSlider1.setRange(0,100)
        self.ui.pxminSlider1.setValue(0)
        self.ui.pxminLabel1.setText(str("%d" %0 )+"%")
        self.ui.pxminSlider1.valueChanged.connect(self.pxminslider1)
        self.ui.pxmaxSlider1.setRange(0,100)
        self.ui.pxmaxSlider1.setValue(100)
        self.ui.pxmaxLabel1.setText(str("%d" %100 )+"%")
        self.ui.pxmaxSlider1.valueChanged.connect(self.pxmaxslider1)
        self.ui.pxminSlider2.setRange(0,100)
        self.ui.pxminSlider2.setValue(0)
        self.ui.pxminLabel2.setText(str("%d" %0 )+"%")
        self.ui.pxminSlider2.valueChanged.connect(self.pxminslider2)
        self.ui.pxmaxSlider2.setRange(0,100)
        self.ui.pxmaxSlider2.setValue(100)
        self.ui.pxmaxLabel2.setText(str("%d" %100 )+"%")
        self.ui.pxmaxSlider2.valueChanged.connect(self.pxmaxslider2)
        # pxmin and pxmax sliders
        self.ui.pyminSlider1.setRange(0,100)
        self.ui.pyminSlider1.setValue(0)
        self.ui.pyminLabel1.setText(str("%d" %0 )+"%")
        self.ui.pyminSlider1.valueChanged.connect(self.pyminslider1)
        self.ui.pymaxSlider1.setRange(0,100)
        self.ui.pymaxSlider1.setValue(100)
        self.ui.pymaxLabel1.setText(str("%d" %100 )+"%")
        self.ui.pymaxSlider1.valueChanged.connect(self.pymaxslider1)
        self.ui.pyminSlider2.setRange(0,100)
        self.ui.pyminSlider2.setValue(0)
        self.ui.pyminLabel2.setText(str("%d" %0 )+"%")
        self.ui.pyminSlider2.valueChanged.connect(self.pyminslider2)
        self.ui.pymaxSlider2.setRange(0,100)
        self.ui.pymaxSlider2.setValue(100)
        self.ui.pymaxLabel2.setText(str("%d" %100 )+"%")
        self.ui.pymaxSlider2.valueChanged.connect(self.pymaxslider2)
        
        self.myplot.ui.widget1.mpl_connect('motion_notify_event', self.motion_notify)
        self.myplot.ui.widget1.mpl_connect('button_press_event', self.onclick1)
        
        self.myplot.ui.widget2.mpl_connect('motion_notify_event', self.motion_notify)
        self.myplot.ui.widget2.mpl_connect('button_press_event', self.onclick2)
                
        self.plotbutton()
    
	# open a new directory and load files for basic information
    def opendir(self):	
        pass
        
    def close(self):
        sys.exit(0)
              
    def backwardtimebutton(self):
        step = self.ui.stepSpinBox.value()
        tstep = self.ui.timeSlider.value()
        tstep = tstep - step
        if tstep < 1:
            tstep = tstep + step
        self.ui.tstepLabel.setText("%d" %tstep)
        time = taxis[tstep-1]    
        self.ui.timeLabel.setText("%6.1f" % time)
        self.ui.timeSlider.setValue(tstep)
        self.plotbutton()
        
    def forwardtimebutton(self):
        step = self.ui.stepSpinBox.value()
        tstep=self.ui.timeSlider.value()
        tstep = tstep + step
        if tstep > tnum:
            tstep = tstep - step
        self.ui.tstepLabel.setText("%d" %tstep)
        time = taxis[tstep-1]    
        self.ui.timeLabel.setText("%6.1f" % time)
        self.ui.timeSlider.setValue(tstep)
        self.plotbutton()
       
    def timeslider(self):
        tstep=self.ui.timeSlider.value()
        time = taxis[tstep-1]
        self.ui.tstepLabel.setText("%d" %tstep)
        self.ui.timeLabel.setText("%6.1f" % time)
        
    def xminslider(self):
        global xmin_factor
        xmin_factor = self.ui.xminSlider.value()
        xmax_factor = self.ui.xmaxSlider.value()
        if xmin_factor >= xmax_factor: 
            xmin_factor = xmax_factor - 1
        self.ui.xminSlider.setValue(xmin_factor)
        xmin = xmin0+(xmax0-xmin0)*xmin_factor/100 
        self.ui.xminLabel.setText(str("%d" %xmin_factor)+"%"
            +" = "+str("%.1f" %xmin))
            
    def xmaxslider(self):
        global xmax_factor
        xmin_factor = self.ui.xminSlider.value()
        xmax_factor = self.ui.xmaxSlider.value()
        if xmax_factor <= xmin_factor: 
            xmax_factor = xmin_factor + 1
        self.ui.xmaxSlider.setValue(xmax_factor)
        xmax = xmin0+(xmax0-xmin0)*xmax_factor/100 
        self.ui.xmaxLabel.setText(str("%d" %xmax_factor)+"%"
            +" = "+str("%.1f" %xmax))
            
    def yminslider(self):
        global ymin_factor
        ymin_factor = self.ui.yminSlider.value()
        ymax_factor = self.ui.ymaxSlider.value()
        if ymin_factor >= ymax_factor:
            ymin_factor = ymax_factor - 1
        self.ui.yminSlider.setValue(ymin_factor)
        ymin = ymin0+(ymax0-ymin0)*ymin_factor/100 
        self.ui.yminLabel.setText(str("%d" %ymin_factor)+"%"
            +" = "+str("%.1f" %ymin))
            
    def ymaxslider(self):
        global ymax_factor
        ymin_factor = self.ui.yminSlider.value()
        ymax_factor = self.ui.ymaxSlider.value()
        if ymax_factor <= ymin_factor: 
            ymax_factor = ymin_factor + 1
        self.ui.ymaxSlider.setValue(ymax_factor)
        ymax = ymin0+(ymax0-ymin0)*ymax_factor/100
        self.ui.ymaxLabel.setText(str("%d" %ymax_factor)+"%"
            +" = "+str("%.1f" %ymax))
        
    def fieldcombobox1(self):
        self.ui.fieldButton1.setChecked(True)
        self.plotbutton()
    def fieldcombobox2(self):
        self.ui.fieldButton2.setChecked(True)
        self.plotbutton()
        
    def particlecombobox1(self):
        self.ui.particleButton1.setChecked(True)
        self.plotbutton()    
    def particlecombobox2(self):
        self.ui.particleButton2.setChecked(True)
        self.plotbutton()

    def pngcheckbox(self):
        if self.ui.pngCheckBox.isChecked():
            self.ui.epsCheckBox.setChecked(False)
            self.ui.movieCheckBox.setChecked(False)
            
    def epscheckbox(self):
        if self.ui.epsCheckBox.isChecked():
            self.ui.pngCheckBox.setChecked(False)
            self.ui.movieCheckBox.setChecked(False)
            
    def moviecheckbox(self):
        if self.ui.movieCheckBox.isChecked():
            self.ui.pngCheckBox.setChecked(False)
            self.ui.epsCheckBox.setChecked(False)
          
            os.system('mkdir ../movie')
                
    def plotbutton(self):
            
        if animation == False:
        
            tstep = self.ui.timeSlider.value()
            index=self.ui.fieldsComboBox1.currentIndex()
            field1 = field_list[index]
            index=self.ui.particlesComboBox1.currentIndex()
            species1 = species_list[index]
            index=self.ui.phaseComboBox1.currentIndex()
            phase1 = phase_list[index]
            index=self.ui.localphaseComboBox1.currentIndex()
            localphase1 = localphase_list[index]
            temp1 = float(self.ui.TempEdit1.toPlainText())
            norm1 = float(self.ui.NormEdit1.toPlainText())
            
            index=self.ui.fieldsComboBox2.currentIndex()
            field2 = field_list[index]
            index=self.ui.particlesComboBox2.currentIndex()
            species2 = species_list[index]
            index=self.ui.phaseComboBox2.currentIndex()
            phase2 = phase_list[index]
            index=self.ui.localphaseComboBox2.currentIndex()
            localphase2 = localphase_list[index]
            temp2 = float(self.ui.TempEdit2.toPlainText())
            norm2 = float(self.ui.NormEdit2.toPlainText())
            
            if self.ui.pngCheckBox.isChecked(): saveimg = 1
            elif self.ui.epsCheckBox.isChecked(): saveimg = 2
            else:   saveimg = 0
            
            if self.ui.fieldButton1.isChecked():
                self.myplot.ui.widget1.field(tstep, field1, oned1, aspect1, localphase1, count1,
                        xi1_factor, yi1_factor, xf1_factor, yf1_factor, lineselect1, contrast1,
                        pxmin1, pxmax1, pymin1, pymax1, saveimg, window1)
            if self.ui.particleButton1.isChecked():
                self.myplot.ui.widget1.plotparticle(tstep, species1, phase1, aspect1, localphase1, count1, 
                        xi1_factor, yi1_factor, xf1_factor, yf1_factor, lineselect1, tempfit1, temp1, norm1, 
                        pxmin1, pxmax1, pymin1, pymax1, saveimg, window1)
            if self.ui.fieldButton2.isChecked():
                self.myplot.ui.widget2.field(tstep, field2, oned2, aspect2, localphase2, count2, 
                        xi2_factor, yi2_factor, xf2_factor, yf2_factor, lineselect2, contrast2,
                        pxmin2, pxmax2, pymin2, pymax2, saveimg, window2)
            if self.ui.particleButton2.isChecked():
                self.myplot.ui.widget2.plotparticle(tstep, species2, phase2, aspect2, localphase2, count2, 
                        xi2_factor, yi2_factor, xf2_factor, yf2_factor, lineselect2, tempfit2, temp2, norm2,
                        pxmin2, pxmax2, pymin2, pymax2, saveimg, window2)
        
    def myEvenListener(self):
        global animation
        animation = True
        
        index=self.ui.fieldsComboBox1.currentIndex()
        field1 = field_list[index]
        index=self.ui.particlesComboBox1.currentIndex()
        species1 = species_list[index]
        index=self.ui.phaseComboBox1.currentIndex()
        phase1 = phase_list[index]
        index=self.ui.localphaseComboBox1.currentIndex()
        localphase1 = localphase_list[index]
        temp1 = float(self.ui.TempEdit1.toPlainText())
        norm1 = float(self.ui.NormEdit1.toPlainText())
        
        index=self.ui.fieldsComboBox2.currentIndex()
        field2 = field_list[index]
        index=self.ui.particlesComboBox2.currentIndex()
        species2 = species_list[index]
        index=self.ui.phaseComboBox2.currentIndex()
        phase2 = phase_list[index]
        index=self.ui.localphaseComboBox2.currentIndex()
        localphase2 = localphase_list[index]
        temp2 = float(self.ui.TempEdit2.toPlainText())
        norm2 = float(self.ui.NormEdit2.toPlainText())
        
        if self.ui.movieCheckBox.isChecked(): 
            saveimg = 3
            image_dir = '..'
            if self.ui.fieldButton1.isChecked():
                os.system('mkdir '+image_dir+'/movie/'+field1+'_'+localphase1)
            else:
                os.system('mkdir '+image_dir+'/movie/'+species1+'_'+phase1+'_'+localphase1)
            if self.ui.fieldButton2.isChecked():
                os.system('mkdir '+image_dir+'/movie/'+field2+'_'+localphase2)
            else:
                os.system('mkdir '+image_dir+'/movie/'+species2+'_'+phase2+'_'+localphase2)
                
        else: saveimg = 0
        global tini, tmax
        tini = self.ui.tiniSpinBox.value()
        tmax = self.ui.tmaxSpinBox.value()
        step = self.ui.stepSpinBox.value()
        # number of iterations in animation
        num = (tmax-tini)/step+1
        if num<0: num=1
        tarr = np.zeros(num, dtype=np.int)
        t = tmax
        for i in np.arange(num)[::-1]:
            tarr[i] = t
            t = t - step
        for tstep in tarr:
            tm.sleep(0.005)
            self.ui.tstepLabel.setText("%d" %tstep)
            self.ui.timeLabel.setText("%6.1f $/\omega_{pe}$" % time)
            self.ui.timeSlider.setValue(tstep)
            if self.ui.fieldButton1.isChecked():
                self.myplot.ui.widget1.field(tstep, field1, oned1, aspect1, localphase1, count1, 
                        xi1_factor, yi1_factor, xf1_factor, yf1_factor, lineselect1, contrast1,
                        pxmin1, pxmax1, pymin1, pymax1, saveimg, window1)
            if self.ui.particleButton1.isChecked():
                self.myplot.ui.widget1.plotparticle(tstep, species1, phase1, aspect1, localphase1, count1, 
                        xi1_factor, yi1_factor, xf1_factor, yf1_factor, lineselect1, tempfit1, temp1, norm1, 
                        pxmin1, pxmax1, pymin1, pymax1, saveimg, window1)
            if self.ui.fieldButton2.isChecked():
                self.myplot.ui.widget2.field(tstep, field2, oned2, aspect2, localphase2, count2, 
                        xi2_factor, yi2_factor, xf2_factor, yf2_factor, lineselect2, contrast2,
                        pxmin2, pxmax2, pymin2, pymax2, saveimg, window2)
            if self.ui.particleButton2.isChecked():
                self.myplot.ui.widget2.plotparticle(tstep, species2, phase2, aspect2, localphase2, count2, 
                        xi2_factor, yi2_factor, xf2_factor, yf2_factor, lineselect2, tempfit2, temp2, norm2,
                        pxmin2, pxmax2, pymin2, pymax2, saveimg, window2)
            tstep = tstep + step
        # When animation is done, animation = False
        animation = False
        
    def animationbutton(self):
        # while animation is running, this function is skipped.
        if animation == False:
            self.c_thread=threading.Thread(target=self.myEvenListener)
            self.c_thread.start()
        else:
            pass
                        
    def motion_notify(self,event):
        imx, imy = event.xdata, event.ydata
        if imx != None and imy != None:
            self.ui.coordLabel.setText("(x1,x2)=(%4.2f, %4.2f)" %(imx, imy))
            
    def onedcheckbox1(self):
        global oned1
        if self.ui.onedCheckBox1.isChecked():
            oned1 = True
        else:
            oned1 = False
    def onedcheckbox2(self):
        global oned2
        if self.ui.onedCheckBox2.isChecked():
            oned2 = True
        else:
            oned2 = False
            
    def aspectcheckbox1(self):
        global aspect1
        if self.ui.aspectCheckBox1.isChecked():
            aspect1='equal'
        else:
            aspect1='auto'
            
    def aspectcheckbox2(self):
        global aspect2
        if self.ui.aspectCheckBox2.isChecked():
            aspect2='equal'
        else:
            aspect2='auto'
            
    def linecheckbox1(self):
        global lineselect1, count1
        if self.ui.lineCheckBox1.isChecked() == False:
            count1 = 0
            self.plotbutton()
        else:
            lineselect1 = True
            self.ui.rectangleCheckBox1.setChecked(False)                      
    def linecheckbox2(self):
        global lineselect2, count2
        if self.ui.lineCheckBox2.isChecked() == False:
            count2 = 0
            self.plotbutton()
        else:
            lineselect2 = True
            self.ui.rectangleCheckBox2.setChecked(False)        
                                  
    def rectanglecheckbox1(self):
        global lineselect1, count1
        if self.ui.rectangleCheckBox1.isChecked() == False:
            count1 = 0
            self.plotbutton()
        else:
            lineselect1 = False
            self.ui.lineCheckBox1.setChecked(False)
    def rectanglecheckbox2(self):
        global lineselect2, count2
        if self.ui.rectangleCheckBox2.isChecked() == False:
            count2 = 0
            self.plotbutton()
        else:
            lineselect2 = False
            self.ui.lineCheckBox2.setChecked(False)

    def onclick1(self, event):
        if self.ui.fieldButton1.isChecked():
            if oned1 == True: pass
        if self.ui.particleButton1.isChecked():
            index=self.ui.phaseComboBox1.currentIndex()
            phase1 = phase_list[index]
            if phase1 != 'x-y': pass
        if self.ui.rectangleCheckBox1.isChecked() or self.ui.lineCheckBox1.isChecked():
            global count1
            global xi1_factor, xf1_factor, yi1_factor, yf1_factor
            if count1 == 2: count1 = 0
            imx, imy = event.xdata, event.ydata
            if imx != None and imy != None:
                count1 += 1
            if count1 == 1:
                xi1_factor = 1.*(imx - xmin0)/(xmax0-xmin0)
                yi1_factor = 1.*(imy - ymin0)/(ymax0-ymin0)
                self.plotbutton()
            if count1 == 2:
                xf1_factor = 1.*(imx - xmin0)/(xmax0-xmin0)
                yf1_factor = 1.*(imy - ymin0)/(ymax0-ymin0)
                self.plotbutton()
                             
    def onclick2(self, event):
        if self.ui.fieldButton2.isChecked():
            if oned2 == True: pass
        if self.ui.particleButton2.isChecked():
            index=self.ui.phaseComboBox2.currentIndex()
            phase2 = phase_list[index]
            if phase2 != 'x-y': pass
        if self.ui.rectangleCheckBox2.isChecked() or self.ui.lineCheckBox2.isChecked():
            global count2
            global xi2_factor, xf2_factor, yi2_factor, yf2_factor
            if count2 == 2: count2 = 0
            imx, imy = event.xdata, event.ydata
            if imx != None and imy != None:
                count2 += 1
            if count2 == 1:
                xi2_factor = 1.*(imx - xmin0)/(xmax0-xmin0)
                yi2_factor = 1.*(imy - ymin0)/(ymax0-ymin0)
                self.plotbutton()
            if count2 == 2:
                xf2_factor = 1.*(imx - xmin0)/(xmax0-xmin0)
                yf2_factor = 1.*(imy - ymin0)/(ymax0-ymin0)
                self.plotbutton()
                
    def localdatapushbutton1(self):
        if count1 == 2: 
            self.plotbutton()
    def localdatapushbutton2(self):
        if count2 == 2:
            self.plotbutton()
            
    def t2dcheckbox1(self):
        global tempfit1
        if self.ui.T2dCheckBox1.isChecked() == True:
            self.ui.T3dCheckBox1.setChecked(False)
            tempfit1 = 1
        else: 
            tempfit1 = 0
    def t2dcheckbox2(self):
        global tempfit2
        if self.ui.T2dCheckBox2.isChecked() == True:
            self.ui.T3dCheckBox2.setChecked(False)
            tempfit2 = 1
        else: 
            tempfit2 = 0
    def t3dcheckbox1(self):
        global tempfit1
        if self.ui.T3dCheckBox1.isChecked() == True:
            self.ui.T2dCheckBox1.setChecked(False)
            tempfit1 = 2            
        else:
            tempfit1 = 0
    def t3dcheckbox2(self):
        global tempfit2
        if self.ui.T3dCheckBox2.isChecked() == True:
            self.ui.T2dCheckBox2.setChecked(False)
            tempfit2 = 2
        else:
            tempfit2 = 0
            
    def contrastslider1(self):
        global contrast1
        contrast1 = self.ui.contrastSlider1.value()
        self.ui.contrastLabel1.setText(str("%d" %contrast1)+"%")
    
    def contrastslider2(self):
        global contrast2
        contrast2 = self.ui.contrastSlider2.value()
        self.ui.contrastLabel2.setText(str("%d" %contrast2)+"%")
        
    def returncontrastbutton2(self):
        global contrast2
        contrast2 = 100
        self.ui.contrastSlider2.setValue(contrast2)
        self.ui.contrastLabel2.setText(str("%d" %contrast2)+"%")
        self.plotbutton()
        
    def returncontrastbutton1(self):
        global contrast1
        contrast1 = 100
        self.ui.contrastSlider1.setValue(contrast1)
        self.ui.contrastLabel1.setText(str("%d" %contrast1)+"%")    
        self.plotbutton()
            
    def pxminslider1(self):
        global pxmin1
        pxmin1 = self.ui.pxminSlider1.value()
        pxmax1 = self.ui.pxmaxSlider1.value()
        if pxmin1 >= pxmax1: 
            pxmin1 = pxmax1 - 1
        self.ui.pxminSlider1.setValue(pxmin1)
        self.ui.pxminLabel1.setText(str("%d" %pxmin1)+"%")
                                   
    def pxmaxslider1(self):
        global pxmax1
        pxmin1 = self.ui.pxminSlider1.value()
        pxmax1 = self.ui.pxmaxSlider1.value()
        if pxmax1 <= pxmin1: 
            pxmax1 = pxmin1 + 1
        self.ui.pxmaxSlider1.setValue(pxmax1)
        self.ui.pxmaxLabel1.setText(str("%d" %pxmax1)+"%")
            
    def pyminslider1(self):
        pymin1 = self.ui.pyminSlider1.value()
        pymax1 = self.ui.pymaxSlider1.value()
        if pymin1 >= pymax1: 
            pymin1 = pymax1 - 1
        self.ui.pyminSlider1.setValue(pymin1)
        self.ui.pyminLabel1.setText(str("%d" %pymin1)+"%")
                
    def pymaxslider1(self):
        global pymax1
        pymin1 = self.ui.pyminSlider1.value()
        pymax1 = self.ui.pymaxSlider1.value()
        if pymax1 <= pymin1: 
            pymax1 = pymin1 + 1
        self.ui.pymaxSlider1.setValue(pymax1)
        self.ui.pymaxLabel1.setText(str("%d" %pymax1)+"%")

    def pxminslider2(self):
        global pxmin2
        pxmin2 = self.ui.pxminSlider2.value()
        pxmax2 = self.ui.pxmaxSlider2.value()
        if pxmin2 >= pxmax2: 
            pxmin2 = pxmax2 - 1
        self.ui.pxminSlider2.setValue(pxmin2)
        self.ui.pxminLabel2.setText(str("%d" %pxmin2)+"%")
                
    def pxmaxslider2(self):
        global pxmax2
        pxmin2 = self.ui.pxminSlider2.value()
        pxmax2 = self.ui.pxmaxSlider2.value()
        if pxmax2 <= pxmin2: 
            pxmax2 = pxmin2 + 1
        self.ui.pxmaxSlider2.setValue(pxmax2)
        self.ui.pxmaxLabel2.setText(str("%d" %pxmax2)+"%")
            
    def pyminslider2(self):
        global pymin2
        pymin2 = self.ui.pyminSlider2.value()
        pymax2 = self.ui.pymaxSlider2.value()
        if pymin2 >= pymax2: 
            pymin2 = pymax2 - 1
        self.ui.pyminSlider2.setValue(pymin2)
        self.ui.pyminLabel2.setText(str("%d" %pymin2)+"%")
            
    def pymaxslider2(self):
        global pymax2
        pymin2 = self.ui.pyminSlider2.value()
        pymax2 = self.ui.pymaxSlider2.value()
        if pymax2 <= pymin2: 
            pymax2 = pymin2 + 1
        self.ui.pymaxSlider2.setValue(pymax2)
        self.ui.pymaxLabel2.setText(str("%d" %pymax2)+"%")
        
        
class MatplotlibWidget(Canvas):
    
    def __init__(self, parent=None):		
        super(MatplotlibWidget, self).__init__(Figure())
        
        self.setParent(parent)
        self.figure = Figure(dpi=60)
        self.canvas = Canvas(self.figure)
        self.figure.set_facecolor('0.85')
        
    def field(self, tstep, field, oned, aspect, localphase, 
                count, xi_factor, yi_factor, xf_factor, yf_factor, lineselect, contrast,
                pxmin, pxmax, pymin, pymax, saveimg, window):
                                                         
        self.figure.clear()
        
        global xmin0, xmax0, ymin0, ymax0
        ########################################
        # read hdf5 field data
        fi = h5py.File(fld_list[tstep-1], 'r')
        fdata = fi[field]
        fdata = fdata[0,:,:]
        ########################################
        ny, nx = np.shape(fdata)
        
        
        xmin0 = 0; xmax0 = nx*istep*dx
        ymin0 = 0; ymax0 = ny*istep*dx
        
        xaxis=1.*np.arange(nx)*(xmax0-xmin0)/(nx-1)+xmin0
        yaxis=1.*np.arange(ny)*(ymax0-ymin0)/(ny-1)+ymin0
         
        # zoom-in/out
        xmin = xmin0+(xmax0-xmin0)*xmin_factor/100
        xmax = xmin0+(xmax0-xmin0)*xmax_factor/100
        ymin = ymin0+(ymax0-ymin0)*ymin_factor/100 
        ymax = ymin0+(ymax0-ymin0)*ymax_factor/100
        
        # zoom-in/out indices
        iL = np.where(xaxis >= xmin)[0]
        if len(iL) != 0: iL=iL[0]
        else: iL = 0
        iR = np.where(xaxis >= xmax)[0]
        if len(iR) != 0: iR=iR[0]
        else: iR = nx-1
        jL = np.where(yaxis >= ymin)[0]
        if len(jL) != 0: jL=jL[0]
        else: jL = 0
        jR = np.where(yaxis >= ymax)[0]
        if len(jR) != 0: jR=jR[0]
        else: jR = ny-1

        # selection of a local zone
        xi = xmin0+(xmax0-xmin0)*xi_factor
        yi = ymin0+(ymax0-ymin0)*yi_factor
        xf = xmin0+(xmax0-ymin0)*xf_factor 
        yf = ymin0+(ymax0-ymin0)*yf_factor
        
        nbin = 500
        fontsize0 = 14; fontsize1 = 13
        cbarwidth = 0.16
        
        
        # Main plot
        if count == 0:
            self.plot = self.figure.add_subplot(111)
        else:
            self.plot = self.figure.add_subplot(121)
            
        if oned:        
            jm=int((jL+jR)/2.)
            self.plot.cla()
            self.plot.plot(xaxis, fdata[jm,:])
            if saveimg == 0:
                self.plot.set_title(field, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(field+'   t=%6.1f ' % time, fontsize=fontsize0)
            self.plot.set_xlabel('x ($c/\omega_{pe}$)', fontsize=fontsize1)
            self.plot.set_aspect('auto')
            self.plot.axes.set_xlim([xmin,xmax])
            self.draw()
        
        else: 
            self.plot.cla()
            vmin = np.min(fdata[jL:jR,iL:iR])
            vmin = vmin*contrast/100.
            vmax = np.max(fdata[jL:jR,iL:iR])
            vmax = vmax*contrast/100.
            im =  self.plot.imshow(fdata[jL:jR,iL:iR], interpolation='nearest', cmap='jet',
                origin='lower', extent=[ xmin,xmax,ymin,ymax ], vmin=vmin, vmax=vmax, aspect=aspect)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im, cax=cax)
            if saveimg == 0:
                self.plot.set_title(field, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(field+'   t=%6.1f $/\omega_{pe}$' % time, fontsize=fontsize0)
            self.plot.set_xlabel('x ($c/\omega_{pe}$)', fontsize=fontsize1)
            self.plot.set_ylabel('y ($c/\omega_{pe}$)', fontsize=fontsize1)
            self.plot.axes.set_xlim([xmin,xmax])
            self.plot.axes.set_ylim([ymin,ymax])
            self.plot.axes.get_figure().tight_layout()
            if count == 1:
                self.plot.plot([xi, xi],[yi, yi], 'o', color='blue')
            if count == 2:
                if localphase == 'spece2D' or localphase == 'spece2Drest'   \
                    or localphase == 'speci2D' or localphase == 'speci2Drest'   \
                    or localphase == 'spece' or localphase == 'specerest'   \
                    or localphase == 'speci' or localphase == 'specirest':
                
                    if lineselect:
                        self.plot.plot([xi, xi], [ymin0,ymax0], ':', color='black')
                        self.plot.plot([xf, xf], [ymin0,ymax0], ':', color='black')
                    else:
                        self.plot.plot([xi, xi], [yi, yi], '.', color='blue')
                        self.plot.plot([xi, xf], [yi, yi], ':', color='black')
                        self.plot.plot([xi, xf], [yf, yf], ':', color='black')
                        self.plot.plot([xi, xi], [yi, yf], ':', color='black')
                        self.plot.plot([xf, xf], [yi, yf], ':', color='black')
                        self.plot.plot([xf, xf], [yf, yf], '.', color='red')
                else:
                    if lineselect:
                        self.plot.plot([xi, xi], [yi, yi], '.', color='blue')
                        self.plot.plot([xi, xf], [yi, yf], ':', color='black')
                        self.plot.plot([xf, xf], [yf, yf], '.', color='red')
                    else:
                        self.plot.plot([xi, xi], [yi, yi], '.', color='blue')
                        self.plot.plot([xi, xf], [yi, yi], ':', color='black')
                        self.plot.plot([xi, xf], [yf, yf], ':', color='black')
                        self.plot.plot([xi, xi], [yi, yf], ':', color='black')
                        self.plot.plot([xf, xf], [yi, yf], ':', color='black')
                        self.plot.plot([xf, xf], [yf, yf], '.', color='red')
                
            self.draw()
            
        #############################################
        # Local field plot
        #############################################
        if count == 2:    
            self.plot = self.figure.add_subplot(122)
            self.plot.cla()
            xmin1 = np.min([xi,xf])
            xmax1 = np.max([xi,xf])
            ymin1 = np.min([yi,yf])
            ymax1 = np.max([yi,yf])
            
            iL = np.where(xaxis >= xmin1)[0];iL = iL[0]
            iR = np.where(xaxis >= xmax1)[0];iR = iR[0]
            jL = np.where(yaxis >= ymin1)[0];jL = jL[0]
            jR = np.where(yaxis >= ymax1)[0];jR = jR[0]
                       
            if localphase == 'x-y' or localphase == 'FFT':       
                       
                if lineselect:
                    laxis = []
                    ldata = []
                    if iR != iL:
                        m = 1.*(jR-jL)/(iR-iL)
                        for i in np.arange(iL, iR, 1):
                            j = m*(i-iL) + jL
                            j = int(j)
                            ldata.append(fdata[j,i])
                            length = (xaxis[i]-xaxis[iL])**2+(yaxis[j]-yaxis[jL])**2
                            length = length**.5
                            laxis.append(length)
                    else:
                        i = iL
                        for j in np.arange(jL, jR, 1):
                            ldata.append(fdata[j,i])
                            length = (xaxis[i]-xaxis[iL])**2+(yaxis[j]-yaxis[jL])**2
                            length = length**.5
                            laxis.append(length)
                    
                    if localphase == 'x-y': 
                        self.plot.cla()
                        self.plot.plot(laxis, ldata)
                        self.plot.set_title(field, fontsize=fontsize0)
                        self.plot.set_xlabel('$l$ ($c/\omega_{pe}$)', fontsize=fontsize1)
                        self.plot.set_aspect('auto')
                        self.plot.axes.get_figure().tight_layout()
                        self.draw()
                
                    if localphase == 'FFT':
                        nl = len(laxis)
                        lmin1 = laxis[0]
                        lmax1 = laxis[nl-1]
                        lmin = lmin1 - (lmin1+lmax1)*.5
                        lmax = lmax1 - (lmin1+lmax1)*.5 
                        fft = np.fft.fft(ldata)
                        fft = np.abs(np.fft.fftshift(fft))
                        kmin = np.pi/lmin
                        kmax = np.pi/lmax            
                    
                        kmin1 = (kmax-kmin)*pxmin/99+kmin
                        kmax1 = (kmax-kmin)*pxmax/99+kmin  
                        kaxis = np.arange(nl)*(kmax-kmin)/nl+kmin
                    
                        self.plot.cla()
                        self.plot.plot(kaxis, fft)
                        self.plot.set_title(field+' FFT', fontsize=fontsize0)
                        self.plot.set_xlabel('k ($\omega_{pe}/c$)', fontsize=fontsize1)
                        self.plot.set_xlim([kmin1,kmax1])
                        self.plot.set_aspect('auto')
                        self.plot.axes.get_figure().tight_layout()
                        self.draw()
                    
                else: # select rectangle box  
            
                    if localphase == 'x-y':
                        self.plot.cla()
                        im = self.plot.imshow(fdata[jL:jR,iL:iR], interpolation='nearest', cmap='jet',
                                origin='lower', extent=[ xmin1,xmax1,ymin1,ymax1 ], aspect=aspect)
                        ax = self.figure.gca()
                        divider = make_axes_locatable(ax)
                        cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                        cb = self.figure.colorbar(im, cax=cax)
                        self.plot.set_title(field, fontsize=fontsize0)
                        self.plot.set_xlabel('x ($c/\omega_{pe}$)', fontsize=fontsize1)
                        self.plot.set_ylabel('y ($c/\omega_{pe}$)', fontsize=fontsize1)
                        self.plot.axes.set_xlim([xmin1,xmax1])
                        self.plot.axes.set_ylim([ymin1,ymax1])
                        self.plot.axes.get_figure().tight_layout()
                        self.draw()
                
                    if localphase == 'FFT':
                
                        fft2d = np.fft.fft2(fdata[jL:jR,iL:iR])
                        fft2d = np.abs(np.fft.fftshift(fft2d))
            
                        kxmin = np.pi/(xmin1-(xmin1+xmax1)*.5)
                        kxmax = np.pi/(xmax1-(xmin1+xmax1)*.5)
                        kymin = np.pi/(ymin1-(ymin1+ymax1)*.5)
                        kymax = np.pi/(ymax1-(ymin1+ymax1)*.5)
                        ny1,nx1= fft2d.shape
                        kxaxis = np.arange(nx1)*(kxmax-kxmin)/(nx1-1)+kxmin
                        kyaxis = np.arange(ny1)*(kymax-kymin)/(ny1-1)+kymin
            
                        kxmin1 = (kxmax-kxmin)*pxmin/99+kxmin
                        kxmax1 = (kxmax-kxmin)*pxmax/99+kxmin
                        kymin1 = (kymax-kymin)*pymin/99+kymin
                        kymax1 = (kymax-kymin)*pymax/99+kymin
            
                    
                        self.plot.cla()
                        im = self.plot.imshow(fft2d, interpolation='spline16', cmap='jet',
                                origin='lower', extent=[ kxmin,kxmax,kymin,kymax ], aspect=aspect)
                        ax = self.figure.gca()
                        divider = make_axes_locatable(ax)
                        cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                        cb = self.figure.colorbar(im, cax=cax)
                        self.plot.set_title(field+' FFT', fontsize=fontsize0)
                        self.plot.axes.set_xlim([kxmin1,kxmax1])
                        self.plot.axes.set_ylim([kymin1,kymax1])
                        self.plot.set_xlabel('$k_x$ ($\omega_{pe}/c$)', fontsize=fontsize1)
                        self.plot.set_ylabel('$k_y$ ($\omega_{pe}/c$)', fontsize=fontsize1)
                        self.plot.axes.get_figure().tight_layout()
                        self.draw()
                        
            if localphase == 'spece' or localphase == 'speci':
                    
                if localphase == 'spece': dname = 'spece'
                if localphase == 'speci': dname = 'specp'
                
                # read hdf5 spectra data
                sd= h5py.File(spect_list[tstep-1], 'r')
                    
                spect = sd[dname]      # specrum in [gamma-1,x] space
                gamma = sd['gamma']    # gmmma-1 in the lograthmic bin
                xsl   = sd['xsl']      # x-axis of the linear bin
                   
                spect = np.array(spect)
                gamma = np.array(gamma)
                xsl = np.array(xsl)
                xsl = xsl/c_omp 
                xdim = len(xsl)
                gdim = len(gamma)
                
                spect0 = np.zeros((gdim,xdim))
                for i in np.arange(xdim):
                    spect0[:,i] = spect[:,i]*gamma[:]
                spect = spect0
                
                iL = np.where(xsl >= xmin1)[0]; iL=iL[0]
                iR = np.where(xsl >= xmax1)[0]; iR=iR[0]
                       
                # dgamma in the lograthmic bin
                dgamma=np.zeros(gdim)
                delta=np.log10(gamma[gdim-1]/gamma[0])/gdim
                for i in np.arange(gdim):
                    dgamma[i]=gamma[i]*(10**delta-1.)
            
                totnum = np.zeros(xdim)
                for i in np.arange(xdim):
                    totnum[i] = np.sum(spect[:,i])
                        
                # energy distribution, f(E)=(dn/dE)/N
                fe = np.zeros(gdim)
                for k in np.arange(gdim):
                    
                    fe[k] = np.sum(spect[k, iL:iR])/(np.sum(totnum[iL:iR])*dgamma[k])

                
                #if animation == False:
                self.plot.loglog(gamma,fe)
                #self.plot.loglog(bins[:-1], values)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('$\gamma-1$', fontsize=fontsize1)
                self.plot.set_ylabel('$f(\gamma)$', fontsize=fontsize1)
                self.plot.set_aspect('auto')
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                    
                
        if saveimg !=0:
            image_dir = '..'
            if saveimg == 1:
                self.figure.savefig(image_dir+'/'+field+'_'+localphase+'%3.3d.png' %tstep, format='png')  
                print('... png image saved')  
            if saveimg == 2:
                self.figure.savefig(image_dir+'/'+field+'_'+localphase+'%3.3d.eps' %tstep, format='eps', dpi=60)
                print('... eps image saved')  
            if saveimg == 3 and animation == True:
                self.figure.savefig(image_dir+'/movie/'+field+'_'+localphase+'/'+field+'_'+localphase+'%3.3d.png' %tstep, format='png')
  
    
    def plotparticle(self, tstep, species, phase, aspect, localphase, 
                count, xi_factor, yi_factor, xf_factor, yf_factor, lineselect, tempfit, temp, norm, 
                pxmin, pxmax, pymin, pymax, saveimg, window):
                                                                          
        self.figure.clear()
        
        # get min/max domain size
        file = h5py.File(param_list[tstep-1], 'r')
        mx0 = file['mx0'][0];my0 = file['my0'][0];
        xmin0 = 0; xmax0 = mx0*dx
        ymin0 = 0; ymax0 = my0*dx
        
        ########################################
        # read hdf5 particle data
        ########################################
        pdata = h5py.File(prtl_list[tstep-1], 'r')
        if species == 'electron':
            xdata = pdata['xe']    # x position
            ydata = pdata['ye']    # y position
            udata = pdata['ue']    # px momentum (dimensionless)
            vdata = pdata['ve']    # py momentum
            wdata = pdata['we']    # pz momentum
            cdata = pdata['che']   # charge weight
        elif species =='ion':
            xdata = pdata['xi']    # x position
            ydata = pdata['yi']    # y position
            udata = pdata['ui']    # px momentum (dimensionless)
            vdata = pdata['vi']    # py momentum
            wdata = pdata['wi']    # pz momentum
            cdata = pdata['chi']   # charge weight
        ########################################
        cdata = cdata/np.max(cdata)
        xdata = np.array(xdata)/c_omp
        ydata = np.array(ydata)/c_omp
        udata = np.array(udata)
        vdata = np.array(vdata)
        wdata = np.array(wdata)
         
        # zoom-in/out
        xmin = xmin0+(xmax0-xmin0)*xmin_factor/100
        xmax = xmin0+(xmax0-xmin0)*xmax_factor/100
        ymin = ymin0+(ymax0-ymin0)*ymin_factor/100
        ymax = ymin0+(ymax0-ymin0)*ymax_factor/100
           
        if xmin > xmin0 or xmax < xmax0: 
            xloc = np.where((xdata >= xmin) & (xdata <= xmax))[0]
            intersect = xloc
        if ymin > ymin0 or ymax < ymax0:
            yloc = np.where((ydata >= ymin) & (ydata <= ymax))[0]
            intersect = yloc
        if (xmin > xmin0 or xmax < xmax0) and (ymin > ymin0 or ymax < ymax0):
            intersect = np.intersect1d(xloc, yloc)
        if (xmin > xmin0 or xmax < xmax0) or (ymin > ymin0 or ymax < ymax0):
        
            xdata = xdata[intersect]
            ydata = ydata[intersect]
            udata = udata[intersect]
            vdata = vdata[intersect]
            wdata = wdata[intersect]
            cdata = cdata[intersect]
        
        # selection of a local zone
        xi = xmin0+(xmax0-xmin0)*xi_factor
        yi = ymin0+(ymax0-ymin0)*yi_factor
        xf = xmin0+(xmax0-ymin0)*xf_factor 
        yf = ymin0+(ymax0-ymin0)*yf_factor
                   
        nbin = 500
        fontsize0 = 14; fontsize1 = 13
        cbarwidth = 0.16
        
        
        # Main plot
        if count == 0:
            self.plot = self.figure.add_subplot(111)
        else:
            self.plot = self.figure.add_subplot(121)
        
        if phase == 'x-y':
            
            self.plot.cla()
            im = self.plot.hist2d(xdata, ydata, bins=nbin, norm=LogNorm(), weights = cdata)
            self.plot.axes.set_xlim([xmin,xmax])
            self.plot.axes.set_ylim([ymin,ymax])
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im[3], cax=cax)
            
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(species+' '+phase+'   t=%6.1f $/\omega_{pe}$' % time, fontsize=fontsize0)
            self.plot.set_xlabel('x ($c/\omega_{pe}$)', fontsize=fontsize1)
            self.plot.set_ylabel('y ($c/\omega_{pe}$)', fontsize=fontsize1)  
            self.plot.set_aspect(aspect)
            self.plot.axes.get_figure().tight_layout()
            if count == 1:
                self.plot.plot([xi, xi],[yi, yi], '.', color='blue')
            if count == 2: 
                if lineselect:
                    self.plot.plot([xi, xi], [ymin0,ymax0], ':', color='black')
                    self.plot.plot([xf, xf], [ymin0,ymax0], ':', color='black')
                else:
                    self.plot.plot([xi, xi], [yi, yi], '.', color='blue')
                    self.plot.plot([xi, xf], [yi, yi], ':', color='black')
                    self.plot.plot([xi, xf], [yf, yf], ':', color='black')
                    self.plot.plot([xi, xi], [yi, yf], ':', color='black')
                    self.plot.plot([xf, xf], [yi, yf], ':', color='black')
                    self.plot.plot([xf, xf], [yf, yf], '.', color='red')
            self.draw()
               
        if phase == 'f(E)-log-lin':            
            ene = np.sqrt(1.+udata**2+vdata**2+wdata**2)-1.	# gamma-1
            #mass = mass_dic[species]
            #ene = ene * mass * 0.511/massnum
            
            emin = np.min(ene); emax = np.max(ene)
            self.plot.cla()
            self.plot.hist(ene, bins=nbin, log=True, histtype='step', normed=True, weights=cdata)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(species+' '+phase+'   t=%6.1f' % time, fontsize=fontsize0)
            self.plot.set_xlabel('$\gamma-1$', fontsize=fontsize1)
            self.plot.set_ylabel('$f(\gamma)$', fontsize=fontsize1)
            self.plot.set_aspect('auto')
            self.plot.axes.set_xlim([emin,emax])
            self.plot.axes.get_figure().tight_layout()
            self.draw()
            
        if phase == 'f(E)-log-log':
            ene = np.sqrt(1.+udata**2+vdata**2+wdata**2)-1.	# gamma-1
            #mass = mass_dic[species]
            #ene = ene * mass * 0.511/massnum
            
            values, bins = np.histogram(ene, bins=200, normed=True, weights = cdata)
            vmin = np.min(values); vmax = np.max(values)          
            #if animation == False:
            emin = np.min(ene); emax = np.max(ene)
            self.plot.cla()
            self.plot.loglog(bins[:-1], values)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(species+' '+phase+'   t=%6.1f' % time, fontsize=fontsize0)
            self.plot.set_xlabel('$\gamma-1$', fontsize=fontsize1)
            self.plot.set_ylabel('$f(\gamma)$', fontsize=fontsize1)
            self.plot.set_aspect('auto')
            #self.plot.axes.set_xlim([1.e-4,emax])
            self.plot.axes.set_ylim([vmin,vmax])
            self.plot.axes.get_figure().tight_layout()
            self.draw()
            
        if phase == 'x-px':
            self.plot.cla()
            im = self.plot.hist2d(xdata, udata, bins=nbin, norm=LogNorm(), weights = cdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(species+' '+phase+'   t=%6.1f' % time, fontsize=fontsize0)
            self.plot.set_xlabel('x ($c/\omega_{pe}$)', fontsize=fontsize1)
            self.plot.set_ylabel('$p_x (c)$', fontsize=fontsize1)
            self.plot.axes.set_xlim([xmin,xmax])
            self.plot.set_aspect('auto')
            self.plot.axes.get_figure().tight_layout()
            self.draw()    
                
        if phase == 'x-py':    
            self.plot.cla()
            im = self.plot.hist2d(xdata, vdata, bins=nbin, norm=LogNorm(), weights = cdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(species+' '+phase+'   t=%6.1f' % time, fontsize=fontsize0)
            self.plot.set_xlabel('x ($c/\omega_{pe}$)', fontsize=fontsize1)
            self.plot.set_ylabel('$p_y (c)$', fontsize=fontsize1)
            self.plot.axes.set_xlim([xmin,xmax])
            self.plot.set_aspect('auto')
            self.plot.axes.get_figure().tight_layout()
            self.draw()
                
        if phase == 'x-pz':
            self.plot.cla()
            im = self.plot.hist2d(xdata, wdata, bins=nbin, norm=LogNorm(), weights = cdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(species+' '+phase+'   t=%6.1f' % time, fontsize=fontsize0)
            self.plot.set_xlabel('x ($c/\omega_{pe}$)', fontsize=fontsize1)
            self.plot.set_ylabel('$p_z (c)$', fontsize=fontsize1)
            self.plot.axes.set_xlim([xmin,xmax])
            self.plot.set_aspect('auto')
            self.plot.axes.get_figure().tight_layout()
            self.draw()
                
        if phase == 'x-ene':       
            ene = np.sqrt(1.+udata**2+vdata**2+wdata**2)-1.	# gamma-1
            #mass = mass_dic[species]
            #ene = ene * mass * 0.511/massnum
                
            self.plot.cla()
            im = self.plot.hist2d(xdata, ene, bins=nbin, norm=LogNorm(), weights = cdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(species+' '+phase+'   t=%6.1f' % time, fontsize=fontsize0)
            self.plot.set_xlabel('x ($c/\omega_{pe}$)', fontsize=fontsize1)
            self.plot.set_ylabel('$\gamma-1$', fontsize=fontsize1)
            self.plot.axes.set_xlim([xmin,xmax])
            self.plot.set_aspect('auto')
            self.plot.axes.get_figure().tight_layout()
            self.draw()    
                    
        if phase == 'px-py':
            self.plot.cla()
            im = self.plot.hist2d(udata, vdata, bins=nbin, norm=LogNorm(), weights = cdata)
            ax = self.figure.gca()
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size=cbarwidth, pad=0.)
            cb = self.figure.colorbar(im[3], cax=cax)
            if saveimg == 0:
                self.plot.set_title(species+' '+phase, fontsize=fontsize0)
            else:
                time = taxis[tstep-1]
                self.plot.set_title(species+' '+phase+'   t=%6.1f' % time, fontsize=fontsize0)
            self.plot.set_xlabel('$p_x (c)$', fontsize=fontsize1)
            self.plot.set_ylabel('$p_y (c)$', fontsize=fontsize1)
            self.plot.set_aspect(aspect)
            self.plot.axes.get_figure().tight_layout()
            self.draw()
                
            
        #############################################
        # Local plot
        #############################################
        if count == 2:    
            self.plot = self.figure.add_subplot(122)
            self.plot.cla()
            xmin1 = np.min([xi,xf])
            xmax1 = np.max([xi,xf])
            ymin1 = np.min([yi,yf])
            ymax1 = np.max([yi,yf])
                                
            xloc = np.where((xdata > xmin1) & (xdata < xmax1))[0]
            if lineselect:
                intersect = xloc
            else:
                yloc = np.where((ydata > ymin1) & (ydata < ymax1))[0]
                intersect = np.intersect1d(xloc, yloc)
            nselect = len(intersect)
            x = xdata[intersect]
            y = ydata[intersect]
            u = udata[intersect]
            v = vdata[intersect]
            w = wdata[intersect]
            ch = cdata[intersect]
            nbin = 200
            nthres = 10

            if localphase == 'x-y':
                if nselect > nthres:
                    im = self.plot.hist2d(x, y, bins=nbin, norm=LogNorm(), weights = ch)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                if nselect > nthres:
                    cb = self.figure.colorbar(im[3], cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('x $(c/\omega_{pe})$', fontsize=fontsize1)
                self.plot.set_ylabel('y $(c/\omega_{pe})$', fontsize=fontsize1)
                self.plot.axes.set_xlim([xmin1,xmax1])
                if lineselect:
                    self.plot.axes.set_ylim([ymin,ymax])   
                else:
                    self.plot.axes.set_ylim([ymin1,ymax1])     
                self.plot.set_aspect(aspect)
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                
            if localphase == 'density' and nselect > nthres:
        
                values, bins = np.histogram(x, bins=nbin, weights = ch)
                vmin = np.min(values); vmax = np.max(values)
            
                self.plot.plot(bins[:-1], values)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('x $(c/\omega_{pe})$', fontsize=fontsize1)
                self.plot.set_ylabel(localphase, fontsize=fontsize1)
                self.plot.axes.set_xlim([xmin1,xmax1])
                self.plot.axes.set_ylim([vmin,vmax])
                self.plot.set_aspect('auto')
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                
            if localphase == 'spece' or localphase == 'speci':
                    
                if localphase == 'spece': dname = 'spece'
                if localphase == 'speci': dname = 'specp'
                
                # read hdf5 spectra data
                sd= h5py.File(spect_list[tstep-1], 'r')
                    
                spect = sd[dname]      # specrum in [gamma-1,x] space
                gamma = sd['gamma']    # gmmma-1 in the lograthmic bin
                xsl   = sd['xsl']      # x-axis of the linear bin
                   
                spect = np.array(spect)
                gamma = np.array(gamma)
                xsl = np.array(xsl)
                xsl = xsl/c_omp 
                xdim = len(xsl)
                gdim = len(gamma)
                
                spect0 = np.zeros((gdim,xdim))
                for i in np.arange(xdim):
                    spect0[:,i] = spect[:,i]*gamma[:]
                spect = spect0
                
                iL = np.where(xsl >= xmin1)[0]; iL=iL[0]
                iR = np.where(xsl >= xmax1)[0]; iR=iR[0]
                
                # dgamma in the lograthmic bin
                dgamma=np.zeros(gdim)
                delta=np.log10(gamma[gdim-1]/gamma[0])/gdim
                for i in np.arange(gdim):
                    dgamma[i]=gamma[i]*(10**delta-1.)
            
                totnum = np.zeros(xdim)
                for i in np.arange(xdim):
                    totnum[i] = np.sum(spect[:,i])
                        
                # energy distribution, f(E)=(dn/dE)/N
                fe = np.zeros(gdim)
                for k in np.arange(gdim):
                    fe[k] = np.sum(spect[k, iL:iR])/(np.sum(totnum[iL:iR])*dgamma[k])
                    
                #ene = np.sqrt(1.+u**2+v**2+w**2)-1.	# gamma-1
                #values, bins = np.histogram(ene, bins=nbin , normed=True, weights =ch)
                
                #if animation == False:
                self.plot.loglog(gamma,fe)
                #self.plot.loglog(bins[:-1], values)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('$\gamma-1$', fontsize=fontsize1)
                self.plot.set_ylabel('$f(\gamma)$', fontsize=fontsize1)
                self.plot.set_aspect('auto')
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                
                              
            if localphase == 'f(E)-log-lin' and nselect > nthres:
                ene = np.sqrt(1.+u**2+v**2+w**2)-1.	# gamma-1
                #mass = mass_dic[species]
                #ene = ene * mass * 0.511/massnum
                
                #if animation == False:
                emin = np.min(ene); emax = np.max(ene)
                values, bins = np.histogram(ene, bins=nbin , normed=True, weights = ch)
                vmin = np.min(values); vmax = np.max(values)
                
                self.plot.hist(ene, bins=nbin, log=True, histtype='step', normed=True, weights= ch)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('$\gamma-1$', fontsize=fontsize1)
                self.plot.set_ylabel('$f(\gamma)$', fontsize=fontsize1)
                self.plot.set_aspect('auto')
                self.plot.axes.set_ylim([vmin,vmax])
                if tempfit != 0:
                    eaxis = 1.*np.arange(nbin)*(emax-emin)/(nbin-1)+emin
                    gaxis = eaxis+1.
                    if tempfit == 1:
                        fmax = norm*(gaxis+1)*gaxis*np.exp(-(gaxis-1.)/temp)
                    else:
                        fmax = norm*(gaxis+1)*gaxis*np.sqrt(gaxis**2-1)*np.exp(-(gaxis-1.)/temp)
                    self.plot.plot(eaxis, fmax, label='T=%4.1f' %temp)
                    self.plot.legend()
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                
            if localphase == 'f(E)-log-log' and nselect > nthres:
                ene = np.sqrt(1.+u**2+v**2+w**2)-1.	# gamma-1
                #mass = mass_dic[species]
                #ene = ene * mass * 0.511/massnum
                
                #if animation == False:
                emin = np.min(ene); emax = np.max(ene)
                values, bins = np.histogram(ene, bins=nbin , normed=True, weights =ch)
                vmin = np.min(values); vmax = np.max(values)
                
                #if animation == False:
                self.plot.loglog(bins[:-1], values)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('$\gamma-1$', fontsize=fontsize1)
                self.plot.set_ylabel('$f(\gamma)$', fontsize=fontsize1)
                self.plot.set_aspect('auto')
                self.plot.axes.set_xlim([1.e-4,emax])
                self.plot.axes.set_ylim([vmin,vmax])
                if tempfit != 0:
                    eaxis = 1.*np.arange(nbin)*(emax-emin)/(nbin-1)+emin
                    gaxis = eaxis+1.
                    if tempfit == 1:
                        fmax = norm*(gaxis+1)*gaxis*np.exp(-(gaxis-1.)/temp)
                    else:
                        fmax = norm*(gaxis+1)*gaxis*np.sqrt(gaxis**2-1)*np.exp(-(gaxis-1.)/temp)
                    self.plot.plot(eaxis, fmax, label='T=%4.1f' %temp)
                    self.plot.legend()
                self.plot.axes.get_figure().tight_layout()
                self.draw()                
                
            if localphase == 'x-px' and nselect > nthres:
                self.plot.cla()
                im = self.plot.hist2d(x, u, bins=nbin, norm=LogNorm(), weights = ch)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im[3], cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('x $(c/\omega_{pe})$', fontsize=fontsize1)
                self.plot.set_ylabel('$p_x (c)$', fontsize=fontsize1)
                self.plot.axes.set_xlim([xmin1,xmax1])
                self.plot.set_aspect('auto')
                self.plot.axes.get_figure().tight_layout()
                self.draw()    
                
            if localphase == 'x-py' and nselect > nthres:
                
                im = self.plot.hist2d(x, v, bins=nbin, norm=LogNorm(), weights = ch)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im[3], cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('x $(c/\omega_{pe})$', fontsize=fontsize1)
                self.plot.set_ylabel('$p_y (c)$', fontsize=fontsize1)
                self.plot.axes.set_xlim([xmin1,xmax1])
                self.plot.set_aspect('auto')
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                
            if localphase == 'x-pz' and nselect > nthres:
                
                im = self.plot.hist2d(x, w, bins=nbin, norm=LogNorm(), weights = ch)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im[3], cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('x $(c/\omega_{pe})$', fontsize=fontsize1)
                self.plot.set_ylabel('$p_z (c)$', fontsize=fontsize1)
                self.plot.axes.set_xlim([xmin1,xmax1])
                self.plot.set_aspect('auto')
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                
            if localphase == 'x-ene' and nselect > nthres:
                
                ene = np.sqrt(1.+u**2+v**2+w**2)-1.	# gamma-1
                #mass = mass_dic[species]
                #ene = ene * mass * 0.511/massnum
                
                im = self.plot.hist2d(x, ene, bins=nbin, norm=LogNorm(), weights = ch)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im[3], cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('x ($c/\omega_{pe}$)', fontsize=fontsize1)
                self.plot.set_ylabel('$\gamma-1$', fontsize=fontsize1)
                self.plot.axes.set_xlim([xmin1,xmax1])
                self.plot.set_aspect('auto')
                self.plot.axes.get_figure().tight_layout()
                self.draw()    
                    
            if localphase == 'px-py' and nselect > nthres:
            
                im = self.plot.hist2d(u, v, bins=nbin, norm=LogNorm(), weights = ch)
                ax = self.figure.gca()
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size=cbarwidth, pad=0.)
                cb = self.figure.colorbar(im[3], cax=cax)
                self.plot.set_title(localphase, fontsize=fontsize0)
                self.plot.set_xlabel('$p_x (c)$', fontsize=fontsize1)
                self.plot.set_ylabel('$p_y (c)$', fontsize=fontsize1)
                self.plot.set_aspect(aspect)
                self.plot.axes.get_figure().tight_layout()
                self.draw()
                        
            if localphase == 'MaxE' and nselect > nthres:
                
                ene = np.sqrt(1.+udata**2+vdata**2+wdata**2)-1.	# gamma-1
                #mass = mass_dic[species]
                #ene = ene * mass * 0.511/massnum
                
                global maxene1, maxene2
                global tmax_current
                global t_arr1, t_arr2, maxene_arr1, maxene_arr2
                
                # initialize arrays
                if animation == False:
                    tmax_current = tstep
                    if window == 1:
                        t_arr1 = []
                        maxene_arr1 = []
                        maxene1 = np.max(ene)
                    if window == 2:
                        t_arr2 = []
                        maxene_arr2 = []
                        maxene2 = np.max(ene)
                
                if animation == False:
                    
                    self.plot.plot([taxis[tstep-1], taxis[tstep-1]], [np.max(ene), np.max(ene)], 'o', color='blue')
                    self.plot.axes.set_xlim([0,1.2*taxis[tnum-1]])
                    self.plot.axes.set_ylim([0,1.2*np.max(ene)])
                    self.plot.set_title(localphase, fontsize=fontsize0)
                    self.plot.set_xlabel('t $/\omega_{pe}$', fontsize=fontsize1)
                    self.plot.set_ylabel('$gamma-1$', fontsize=fontsize1)
                    self.plot.axes.get_figure().tight_layout()
                    self.draw()
                
                if animation == True: 
                    
                    if window == 1:
                        t_arr1.append(taxis[tstep-1])
                        maxene_arr1.append(np.max(ene))
                    if window == 2:
                        t_arr2.append(taxis[tstep-1])
                        maxene_arr2.append(np.max(ene)) 
                        
                    self.plot.cla()
                    if window == 1:
                        self.plot.plot(t_arr1, maxene_arr1, 'o-', color='blue')
                        self.plot.axes.set_ylim([0,1.2*maxene1])
                    if window == 2:
                        self.plot.plot(t_arr2, maxene_arr2, 'o-', color='blue')    
                        self.plot.axes.set_ylim([0,1.2*maxene2])
                    self.plot.axes.set_xlim([0,1.2*taxis[tmax-1]])
                    self.plot.set_title(localphase, fontsize=fontsize0)
                    self.plot.set_xlabel('t $/\omega_{pe}$', fontsize=fontsize1)
                    self.plot.set_ylabel('$gamma-1$', fontsize=fontsize1)
                    self.plot.axes.get_figure().tight_layout()
                    self.draw()
                    
                # re-initialize arrays after animation has been done
                if tstep == tnum:
                    t_arr = []
                    maxene_arr = []
          
            if localphase == 'data':             
                c = 3.e8
                gamma = np.sqrt(1.+ux**2+uy**2+uz**2) # Lorentz gamma
                vx = ux/gamma*c
                vy = uy/gamma*c
                vz = uz/gamma*c  
                    
                file_name = species+"_t%d_n%d.txt"%(iteration,nselect)
                file = open("../"+file_name, "w")
                for i in np.arange(nselect):
                    file.write(str(z[i]*1.e-6)+"    "+str(x[i]*1.e-6)+"    "+str(vx[i])+"    "+str(vy[i])+"    "+str(vz[i])+"\n")
                file.close()
                print("wrote "+file_name)
                
        if saveimg !=0:
            image_dir = '..'
        
            if saveimg == 1:
                self.figure.savefig(image_dir+'/'+species+'_'+localphase+'%3.3d.png' %tstep, format='png')  
                print('... png image saved')  
            if saveimg == 2:
                self.figure.savefig(image_dir+'/'+species+'_'+localphase+'%3.3d.eps' %tstep, format='eps', dpi=60)
                print('... eps image saved')  
            if saveimg == 3 and animation == True:
                self.figure.savefig(image_dir+'/movie/'+species+'_'+phase+'_'+localphase+'/'+species+'_'+phase+'_'+localphase+'%3.3d.png' %tstep, format='png')
    


if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	myapp = MyForm()
	myapp.show()
	sys.exit(app.exec_())